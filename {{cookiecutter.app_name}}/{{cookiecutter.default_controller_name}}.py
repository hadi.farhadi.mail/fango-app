from fastapi import APIRouter, Depends, Request

from base.messages import NOT_FOUND_ERROR
from {{cookiecutter.app_name}}.{{cookiecutter.default_handler_module_name}} import {{cookiecutter.default_handler_class_name}}




router = APIRouter(
    prefix='/api/{{cookiecutter.app_name}}',
    tags=['{{cookiecutter.app_name.capitalize()}}'],
    responses={404: {'description': NOT_FOUND_ERROR}},
)


def get_handler(request: Request):
    return {{cookiecutter.default_handler_class_name}}(request.app.state.database)


@router.get('/')
async def users(handler: {{cookiecutter.default_handler_class_name}} = Depends(get_handler)):
    # pylint: disable=unused-argument

    return {'message': 'hello from {{cookiecutter.app_name}} app'}
