# import json
import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine

from config import settings
from startup import app
from {{cookiecutter.app_name}}.models import Base

client = TestClient(app)


@pytest.fixture()
def resource():
    engine = create_engine(
        settings.ALEMBIC_CONNECTION_STRING, connect_args={"check_same_thread": False}
    )
    Base.metadata.create_all(bind=engine)
    yield
    Base.metadata.drop_all(bind=engine)


# @pytest.fixture()
# def new_user():
#     return {
#         'username': 'user',
#         'email': 'user@email.com',
#         'password': 'P@S5W0RD'
#     }


class Test{{cookiecutter.app_name.capitalize()}}:

    # pylint: disable=unused-argument
    # pylint: disable=invalid-name

    # Examples
    # @staticmethod
    # def test_register_a_user(resource, new_user):
    #     response = client.post('/api/users/register', data=json.dumps(new_user))
    #     assert response.status_code == 200

    # @staticmethod
    # def test_duplicate_user(resource, new_user):
    #     response = client.post('/api/users/register', data=json.dumps(new_user))

    #     response = client.post('/api/users/register', data=json.dumps(new_user))

    #     assert response.status_code == 400
    pass
