# from datetime import date
# from pydantic import BaseModel, validator, EmailStr


# Example:
# class UserBaseModel(BaseModel):
#     email: EmailStr
#     username: str


# class UserCreateModel(UserBaseModel):
#     password: str

#     @validator('password')
#     @classmethod
#     def password_validation(cls, value):
#         for validator_item in PASSWORD_VALIDATORS:
#             class_validator = get_class(validator_item['class'])
#             class_validator(value, validator_item['data']).validate()

#         return value


# class UserModel(UserBaseModel):
#     id: int
#     fullname: str | None
#     birthday: date | None
#     bio: str | None
#     image_url: str | None
#     is_active: bool
