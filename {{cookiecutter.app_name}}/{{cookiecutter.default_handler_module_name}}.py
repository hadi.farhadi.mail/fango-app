# from sqlalchemy import select

from databases import Database


class {{cookiecutter.default_handler_class_name}}:
    def __init__(self, database: Database):
        self.database = database

    # async def get_users(self) -> List[User]:
    #     query = (
    #         select(User)
    #     )

    #     return await self.database.fetch_all(query)
